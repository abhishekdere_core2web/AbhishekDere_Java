import java.io.*;

class InputUsingBuffeeredReader {
    public static void main(String[] args) throws IOException {

        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);

        System.out.println("Enter Your Name:");
        String name = br.readLine();

        System.out.println("Enter Your Society Name:");
        String Societyname = br.readLine();

        System.out.println("Enter Wing:");
        String wing = br.readLine();

        System.out.println("Enter flat no:");
        String flatno = br.readLine();

        System.out.println("Name : " + name);
        System.out.println("Society name : " + Societyname);
        System.out.println("Wing : " + wing);
        System.out.println("flat no : " + flatno);


    }
}
