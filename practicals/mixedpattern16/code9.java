import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int row=Integer.parseInt(br.readLine());
		for(int i=1;i<=row;i++){
			int ch=64+row-i+1;
			for(int j=1;j<=row-i+1;j++){
				if(i%2==1)
					System.out.print(j+" ");
				else
					System.out.print((char)ch--+" ");
			}
			System.out.println();
		}
	}
}
