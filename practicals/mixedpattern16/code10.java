import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the number:");
		int num=Integer.parseInt(br.readLine());
		int rem=0;
		int rev=0;
		while(num>0){
			rem=num%10;
			if(rem%2==1)
				System.out.print(rem*rem+" ");
			rev=rev*10+rem;
			num=num/10;

		}
		System.out.println();
		System.out.println(rev);
	}
}

