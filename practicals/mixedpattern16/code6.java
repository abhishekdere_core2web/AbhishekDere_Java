import java.util.*;
class Demo{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter number of rows:");
	        int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			int num=row;
			int ch=96+row;
			for(int j=1;j<=i;j++){
				if(i%2==1)
					System.out.print((char)ch--+ " ");
				else
					System.out.print(num-- + " ");
			}
			System.out.println();
		}
	}
}	
