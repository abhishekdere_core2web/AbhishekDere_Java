import java.util.*;
class Array{
	public static void  main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter size:");
		int size=sc.nextInt();
		int arr[]=new int[size];
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}
		//For Even
		int cnt=0;
		System.out.print("even numbers ");
		for(int i=0;i<size;i++){
			if(arr[i]%2==0){
				System.out.print(arr[i]+" ");
				cnt++;
			}
		}
		System.out.println();
		System.out.println("Count of even elements is:"+cnt);
	}
}

