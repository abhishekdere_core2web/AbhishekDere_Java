import java.util.*;
class Array{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter size:");
		int size=sc.nextInt();
		int arr2[]=new int[size];
		for(int i=0;i<arr2.length;i++){
			arr2[i]=sc.nextInt();
		}
		int sum=0;
		System.out.print("Elements divisible by 3: ");
		for(int i=0;i<arr2.length;i++){
			if(arr2[i]%3==0){
				System.out.print(arr2[i]+"\t");
				sum+=arr2[i];
			}
		}
		System.out.println();
		System.out.println("Sum of elements divisible by 3 is:"+sum);
	}
}


