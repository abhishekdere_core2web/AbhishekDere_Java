import java.util.*;
class SpacePattern{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter rows:");
		int row=sc.nextInt();
		int num=1;
		for(int i=1;i<=row;i++){
			int temp=num;
			for(int space=row;space>row-i+1;space--){
				System.out.print(" "+"\t");
			}
			for(int j=1;j<=row-i+1;j++){
				System.out.print(temp+++"\t");
			}
			System.out.println();
			num++;
		}
	}
}	
