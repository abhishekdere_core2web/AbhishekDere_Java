import java.util.*;
class SpacePattern{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter rows:");
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			int num=1;
			for(int space=1;space<=row-i;space++){
				System.out.print(" "+" ");
			}
			for(int j=row-i+1;j<=row;j++){
				System.out.print(num+++" ");
			}
			System.out.println();
		}
	}
}
