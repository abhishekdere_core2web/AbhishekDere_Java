import java.util.*;
class SpacePattern{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter rows:");
		int row=sc.nextInt();
		System.out.println();
		for(int i=1;i<=row;i++){
			int num=64+row-i+1;
			for(int space=1;space<=row-i;space++){
				System.out.print(" "+" ");
			}
			for(int j=row-i+1;j<=row;j++){
				System.out.print((char)num+++" ");
			}

		
			System.out.println();
		}
	}
}
