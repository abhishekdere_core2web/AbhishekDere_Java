import java.util.*;

class Pattern{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		int row=sc.nextInt();
		char ch='A';

		for(int i=1;i<=row;i++){
			char ch1=ch;			
			for(int j=1;j<=i;j++){
				System.out.print(ch1++ +"\t");
			}
			ch++;
			System.out.println();
		}
	}
}	
