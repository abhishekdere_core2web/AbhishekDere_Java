import java.util.*;

class Pattern{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			int num=row;
			for(int j=1;j<=i;j++){
				System.out.print(num*j + "\t");
			}
			System.out.println();
		}
	}
}

