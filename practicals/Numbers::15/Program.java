import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("enter number:");
		int num=Integer.parseInt(br.readLine());
		int factors=1;
		System.out.print("Factors of number are:");
		while(factors<=num){
			if(num%factors==0){
				System.out.print(factors+" ");
			}
			factors++;
		}
	}
}


