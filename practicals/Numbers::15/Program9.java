import java.util.*;
class Demo{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter Number:");
		int num=sc.nextInt();
		int temp=num;
		int rem=0;
		int rev=0;
		while(num>0){
			rem=num%10;
			rev=rev*10+rem;
			num=num/10;			
		}
		if(rev==temp)
			System.out.println("Number is Palindrome");
		else
			System.out.println("Number is not Palindrome");
	}
}
