class WhileDemo6 {
    public static void main(String[] args) {
        char ch = 65; // Initialize ch as character 'A'
        while (ch <= 90) { // Check if ch is less than or equal to 'Z'
            if (ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U') {
               ch++;  // If ch is a vowel, increment ch and continue to the next iteration
                continue;
            }
            System.out.println(ch); // Print ch if it's not a vowel
            ch++; // Increment ch for the next character
        }
    }
}
