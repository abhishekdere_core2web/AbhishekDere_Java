class Array{
	public static void main(String args[]){
		int arr[]={1,4,5,6,11,9,10};
		int prod=1;
		for(int i=0;i<arr.length;i++){
			int temp=arr[i], num=1, cnt=0;
			while(num<=temp){
				if(temp%num==0){
					cnt++;
				}
				num++;
			}
			if(cnt==2){
				prod*=arr[i];
			}
		}
		System.out.println(prod);
	}
}
