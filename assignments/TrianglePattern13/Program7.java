import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int row=Integer.parseInt(br.readLine());
		for(int i=1;i<=row;i++){
			int num=row-i+1;
			int ch=96+row-i+1;
			for(int j=1;j<=row-i+1;j++){
				if(j%2==1)
					System.out.print(num+" ");
				else
					System.out.print((char)ch+" ");
				ch--;
				num--;
			}
			System.out.println();
		}
	}
}
