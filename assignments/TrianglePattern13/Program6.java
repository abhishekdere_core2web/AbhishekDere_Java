import java.util.*;
class Demo{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			int num=1;
			char ch='a';
			for(int j=1;j<=row-i+1;j++){
				if(j%2==1){
					System.out.print(num++ + " ");
				}else{
					System.out.print(ch++ + " ");
				}
			}
			System.out.println();
		}
	}
}
