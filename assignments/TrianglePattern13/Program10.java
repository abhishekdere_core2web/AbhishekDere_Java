import java.util.*;
class Demo{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			int ch1=64+row-i+1;
			int ch2=96+row-i+1;
			if(row%2==1){
				for(int j=1;j<=row-i+1;j++){
					if(i%2==1)
						System.out.print((char)ch2-- +" ");
					else
						System.out.print((char)ch1-- +" ");
				}
				System.out.println();
			}else{
				for(int j=1;j<=row-i+1;j++){
					if(i%2==0)
						System.out.print((char)ch2-- +" ");
					else
						System.out.print((char)ch1-- +" ");
				}
				System.out.println();
			}
		}
	}
}


